#include "tst_unittest.h"
#include "tst_database.h"

#include <gtest/gtest.h>

/**
 * @brief Entry point tests
 *
 * @param argc Number of arguments
 * @param argv List of arguments
 *
 * @return Program exit status
 */
int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
