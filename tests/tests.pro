QT += sql core

include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH *= \
    ../src/

HEADERS += \
        tst_unittest.h \
    ../src/db.h \
    ../src/mainwindow.h \
    ../src/play.h \
    ../src/rec.h \
    ../src/records.h \
    ../src/recordsdb.h \
    tst_database.h

SOURCES += \
        main.cpp \
    ../src/db.cpp \
    ../src/mainwindow.cpp \
    ../src/play.cpp \
    ../src/rec.cpp \
    ../src/records.cpp \
    ../src/recordsdb.cpp

FORMS += \
       ../src/mainwindow.ui \
    ../src/records.ui \
    ../src/play.ui \
    ../src/rec.ui \
    ../src/recordsdb.ui
