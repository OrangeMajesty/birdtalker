#pragma once

/**
 * @class TestDB
 * @ingroup Tests
 * @brief DB class testing
 *
 * The test shows examples of using db class methods.
 *
 * @warning Mock was not used in test methods
 *
 * @author Dmitriy | dmitri.kulakov.000@gmail.com
 * @date 2019/04/05 10:02:00
 *
 */

#include <iostream>
#include <gtest/gtest.h>
#include <db.h>

using namespace testing;


class TestDB : public ::testing::Test
{
protected:

    /// Object Initialization
    void SetUp()
    {
        db = new DB();
        EXPECT_EQ(true,  db->isConnect());
    }

    /// Release of objects
    void TearDown()
    {
        delete db;
    }

    /// Class instance
    DB *db;
};


/**
 * @brief isConnect
 * @warning Test disabled
 * @bug Method isConnect return only false.
 * Test for check db connect
 */
TEST_F(TestDB, DISABLED_isConnect)
{
    /// Stop running tests if there is no connection.
    EXPECT_EQ(true,  db->isConnect());
}
