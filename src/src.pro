#-------------------------------------------------
#
# Project created by QtCreator 2019-03-30T09:16:22
#
#-------------------------------------------------

QT       += core gui sql 

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BirdTalker
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

LIBS += -lgcov

# --coverage option is synonym for: -fprofile-arcs -ftest-coverage -lgcov
QMAKE_CXXFLAGS += --coverage
QMAKE_LFLAGS += --coverage

CONFIG += c++11

SUBDIRS += BirdTalket_Test

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    records.cpp \
    play.cpp \
    rec.cpp \
    recordsdb.cpp \
    db.cpp

HEADERS += \
        mainwindow.h \
    records.h \
    play.h \
    rec.h \
    recordsdb.h \
    db.h

FORMS += \
        mainwindow.ui \
    records.ui \
    play.ui \
    rec.ui \
    recordsdb.ui

CONFIG += mobility
MOBILITY = 


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    img.qrc
