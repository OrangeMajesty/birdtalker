#ifndef REC_H
#define REC_H

#include <QWidget>

namespace Ui {
class Rec;
}

class Rec : public QWidget
{
    Q_OBJECT

public:
    explicit Rec(QWidget *parent = nullptr);
    ~Rec();

private:
    Ui::Rec *ui;
};

#endif // REC_H
