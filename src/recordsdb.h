#ifndef RECORDSDB_H
#define RECORDSDB_H

#include <QWidget>
#include <db.h>

namespace Ui {
class RecordsDB;
}

class RecordsDB : public QWidget
{
    Q_OBJECT

public:
    explicit RecordsDB(QWidget *parent = nullptr);
    ~RecordsDB();

private:
    Ui::RecordsDB *ui;


};

#endif // RECORDSDB_H
