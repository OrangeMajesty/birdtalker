#ifndef PLAY_H
#define PLAY_H

#include <QWidget>

namespace Ui {
class Play;
}

class Play : public QWidget
{
    Q_OBJECT

public:
    explicit Play(QWidget *parent = nullptr);
    ~Play();

private:
    Ui::Play *ui;
};

#endif // PLAY_H
