#ifndef RECORDS_H
#define RECORDS_H

#include <QWidget>
#include <recordsdb.h>

namespace Ui {
class Records;
}

class Records : public QWidget
{
    Q_OBJECT

public:
    explicit Records(QWidget *parent = nullptr);
    ~Records();

private slots:
    void on_pushButton_add_clicked();

private:
    Ui::Records *ui;
};

#endif // RECORDS_H
