#include "records.h"
#include "ui_records.h"

Records::Records(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Records)
{
    ui->setupUi(this);
}

Records::~Records()
{
    delete ui;
}

void Records::on_pushButton_add_clicked()
{
    RecordsDB *recList = new RecordsDB();
    this->hide();
    recList->show();
}
