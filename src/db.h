/**
 * @class DB
 * @brief Class for controll database SQL
 * @author Dmitriy | dmitri.kulakov.000@gmail.com
 * @date 2019/04/05 10:02:00
 *
 * @todo
 *  bugfix isConnect(),
 *
 */

#ifndef DB_H
#define DB_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>

class DB : public QObject
{
    Q_OBJECT

public:
    explicit DB(QObject *parent = nullptr, QString host = "127.0.0.1", int port = 3307, QString login = "root", QString pass = "root1234");

    // Method isConnect() return only false
    bool isConnect();

    QVector<QVector<QString>> query(QString str);

private:
    QSqlDatabase sdb;
};

#endif // DB_H
