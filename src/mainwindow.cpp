#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->centralWidget->setStyleSheet("background: url(':/background/mainwindow')");

    DB *db = new DB();
    db->query("SELECT * FROM thing");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_exit_clicked()
{
    this->close();
}

void MainWindow::on_pushButton_rec_clicked()
{
    Records *rec = new Records();
    this->hide();
    rec->show();
}
