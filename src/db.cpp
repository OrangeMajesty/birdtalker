#include "db.h"
#include <QDebug>
#include <QSqlError>

DB::DB(QObject *parent, QString host, int port, QString login, QString pass) :
    QObject(parent)
{
    sdb = QSqlDatabase::addDatabase("QMYSQL");
    sdb.setHostName(host);
    sdb.setPort(port);
    sdb.setDatabaseName("qt");

    if(!sdb.open(login, pass))
    {
        qDebug() << sdb.lastError().text();
    }
}

/// @bug method isConnect() return only false
bool DB::isConnect()
{
    return sdb.isOpen();
}


QVector<QVector<QString>> DB::query(QString str)
{
    QVector<QVector<QString>> ret;

    if(sdb.isOpen())
    {

        QSqlQuery reply = QSqlQuery(sdb);

        if(!reply.exec(str))
        {
            qDebug() << reply.lastError().driverText();
            qDebug() << reply.lastError().databaseText();
        }

        while(reply.next())
        {
            qDebug() << reply.record();
//            QVector<QString> array;
//            for(int row = 0; row != query.size()+1; row++)
//            {

//                qDebug() << query.value(1).;
//            }
//            ret.push_back();
        }

    }

    return ret;
}
